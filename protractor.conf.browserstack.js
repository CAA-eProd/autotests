var HtmlReporter = require("protractor-beautiful-reporter");
var screenShotUtils = require("protractor-screenshot-utils")
  .ProtractorScreenShotUtils;

exports.config = {
  framework: "jasmine",
  seleniumAddress: "http://hub-cloud.browserstack.com/wd/hub",
  jasmineNodeOpts: {
    defaultTimeoutInterval: 120000,
  },
  restartBrowserBetweenTests: true,
  isBrowserStackConfig: true,
  specs: ["./tests/testcases.login.mobile.spec.js"],

  multiCapabilities: [
    {
      "browserstack.user": "choksiarpigmailc1",
      "browserstack.key": "M66Cx9dEAwPxs6mXE8LW",
      // "browserstack.local": "true",
      device: "iPhone 11 Pro Max",
      browserName: "chrome",
      real_mobile: "true",
      os_version: "13",

    },
    // {
    //   os_version: "9.0",
    //   device: "Samsung Galaxy S10",
    //   real_mobile: "true",
    //   "browserstack.local": "false",
    //   "browserstack.user": "choksiarpigmailc1",
    //   "browserstack.key": "M66Cx9dEAwPxs6mXE8LW",
    //   browserName: "Android"
    // }
  ],

  plugins: [
    {
      package: "protractor-screenshoter-plugin",
      screenshotPath: "./REPORTS/e2e/bs/",
      screenshotOnExpect: "failure+success",
      screenshotOnSpec: "none",
      withLogs: true,
      writeReportFreq: "asap",
      imageToAscii: "none",
      clearFoldersBeforeTest: true,
    },
  ],

  onPrepare: function () {
    browser.ignoreSynchronization = true;

    // returning the promise makes protractor wait for the reporter config before executing tests
    return global.browser.getProcessedConfig().then(function (config) {
      //it is ok to be empty
    });
  },
};

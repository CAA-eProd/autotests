const homePage = require('./pageObjects/HomePage.js');
const loginPage = require('./pageObjects/LoginPage.js');
const fs = require('fs');

describe('Login Feature', function () {
  beforeEach(async function () {
    browser.ignoreSynchronization = true;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;
  });

  it('TC-01-InvalidLogin', function () {
    var EC = browser.ExpectedConditions;

    // Go to home page
    homePage.get();
    expect(true).toEqual(true);

    // Open Mobile Menu
    homePage.clickMobileMenu();
    expect(true).toEqual(true);

    // Click Login
    homePage.clickLogin();
    expect(true).toEqual(true);

    // Enter username/password
    loginPage.enterUserName('asdfsadf'); 
    loginPage.enterPassword('asdfsadf');

    // browser.actions().
    expect('before scroll').toEqual('before scroll');

    browser.executeScript('window.scrollTo(0,300);', 20);
    expect('after scroll').toEqual('after scroll');
    expect('after screen resize').toEqual('after screen resize');

    // Click Login
    loginPage.doLogin();
    expect(true).toEqual(true);

    // Validate result
    browser.wait(EC.presenceOf(element(by.className('validation-summary-errors'))), jasmine.DEFAULT_TIMEOUT_INTERVAL).then(() => {
      element(by.className('validation-summary-errors')).getText().then((validationErrorText) => {
        expect(validationErrorText).toContain("It seems something went wrong, please try again");
      });
    });    
  });

  // it('TC-02-ValidLogin', function () {
  //   var EC = browser.ExpectedConditions;

  //   // Go to home page
  //   homePage.get();
  //   expect(true).toEqual(true);

  //   // Open Mobile Menu
  //   homePage.clickMobileMenu();
  //   expect(true).toEqual(true);

  //   // Click Login
  //   homePage.clickLogin();
  //   expect(true).toEqual(true);

  //   // Enter username/password
  //   loginPage.enterUserName('TestUser1');
  //   loginPage.enterPassword('asdfsadf');
  //   expect('before scroll').toEqual('before scroll');

  //   browser.executeScript('window.scrollTo(0,300);', 20);
  //   expect('after scroll').toEqual('after scroll');
  //   expect('after screen resize').toEqual('after screen resize');

  //   // Click Login
  //   loginPage.doLogin();
  //   expect(true).toEqual(true);

  //   // Validate result
  //   browser.wait(EC.presenceOf(element(by.className('sidebar__section-link--active'))), jasmine.DEFAULT_TIMEOUT_INTERVAL).then(() => {
  //     element(by.className('account-page-header__heading')).getText().then((overViewTxt) => {
  //       expect(overViewTxt).toEqual('Overview.');
  //     });
  //   });
  // });
});

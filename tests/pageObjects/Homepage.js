const HomePage = function () {
  this.get = function () {
    browser.driver.get('https://qa1.test.caasco.ca/');
  };

  this.clickLogin = function () {
    var EC = browser.ExpectedConditions;

    //Wait for Login link to be visible 
    browser.wait(EC.elementToBeClickable(element(by.linkText('Login'))), 10000).then(() => {

      //Click Login link
      element(by.linkText('Login')).click();
    });
  }

  this.clickMobileMenu = function () {
    var EC = browser.ExpectedConditions;

    //Wait for Login link to be visible 
    browser.wait(EC.elementToBeClickable(element(by.linkText('Menu'))), 10000).then(() => {

      //Click Login link
      element(by.linkText('Menu')).click();
    });
  }
};

module.exports = new HomePage();
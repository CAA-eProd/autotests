const LoginPage = function () {
  var EC = browser.ExpectedConditions;

  this.enterUserName = (userName) => {
    return new Promise((resolve, reject) => {
      browser.wait(EC.presenceOf(element(by.name('UserIdentity'))), 5000).then(() => {
        element(by.name('UserIdentity')).sendKeys(userName);
        resolve();
      });
    });
  }

  this.enterPassword = (password) => {
    return new Promise((resolve, reject) => {
      browser.wait(EC.presenceOf(element(by.name('Password'))), 5000).then(() => {
        element(by.name('Password')).sendKeys(password);
        resolve();
      });
    });
  }

  this.doLogin = () => {
    return new Promise((resolve, reject) => {
      element(by.xpath('//button[text()="Login"]')).click();
      resolve();
    });
  }
};

module.exports = new LoginPage();
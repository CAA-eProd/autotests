const homePage = require('./pageObjects/HomePage.js');
const loginPage = require('./pageObjects/LoginPage.js');
const fs = require('fs');
// const testHelper = require('./TestHelper');

describe('Login Feature', function () {
  beforeEach(async function () { 
    browser.ignoreSynchronization = true;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;
    browser.driver.manage().window().setSize(1024, 2000);
  });

  it('TC-01-InvalidLogin', function () {
    const testCase = 'TC-01';
    var screenShotCounter = 1;
    var EC = browser.ExpectedConditions;

    homePage.get();
    takeScreenShot(true, true);
    homePage.clickLogin();
    takeScreenShot(true, true);
    loginPage.enterUserName('asdfsadf');
    loginPage.enterPassword('asdfsadf');
    takeScreenShot(true, true);
    loginPage.doLogin();
    takeScreenShot(true, true);

    browser.wait(EC.presenceOf(element(by.className('validation-summary-errors'))), jasmine.DEFAULT_TIMEOUT_INTERVAL).then(() => {
      element(by.className('validation-summary-errors')).getText().then((validationErrorText) => {
        takeScreenShot(true, true);
        expect(validationErrorText).toContain("It seems something went wrong, please try again");
      });
    });
  });

  it('TC-02-Valid Login', function () {
    homePage.get();
    takeScreenShot(true, true);
    homePage.clickLogin();
    takeScreenShot(true, true);
    loginPage.enterUserName('TestUser1');
    loginPage.enterPassword('Test1234');
    takeScreenShot(true, true);
    loginPage.doLogin();
    takeScreenShot(true, true);
    
    var EC = browser.ExpectedConditions;

    browser.wait(EC.presenceOf(element(by.className('sidebar__section-link--active'))), jasmine.DEFAULT_TIMEOUT_INTERVAL).then(() => {
      element(by.className('account-page-header__heading')).getText().then((overViewTxt) => {
        takeScreenShot(overViewTxt, 'Overview.');
      });
    });
  });
});

function takeScreenShot(expected, actual) {
  browser.executeScript('if(document.getElementsByClassName("scroll-container")[0]) {document.getElementsByClassName("scroll-container")[0].setAttribute("scrollH", document.getElementsByClassName("scroll-container")[0].scrollHeight)}').then(()=> {
    element(by.className("scroll-container")).getAttribute("scrollH").then(scrollH => {
      browser.driver.manage().window().setSize(1024, parseInt(scrollH,10));
      expect(expected).toEqual(actual);
    });
  });
}
## Setup

Install libraries/dependencies

```
npm install
npm install protractor -g
npm install protractor-screenshoter-plugin -g
```

## Update Webdriver
```
npm run do-setup
```
This will install the following libraries:
- protractor

## Install Microsoft Edge webdriver (to test with Edge)
download MicrosoftWebDriver.exe from https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/
Start Edge Webdriver (Old Edge)
```
webdriver-manager start --edge MicrosoftWebDriver.exe
```

Start Edge Webdriver (New Edge - Chromium)
```
webdriver-manager start --edge msedgedriver.exe
```

## Running Tests

Run tests locally
```
npm test
```

# Download And start BrowserStackLocal

Download and unzip BrowserStack Local in a folder (e.g. C:\Tools\)
https://www.browserstack.com/browserstack-local/BrowserStackLocal-win32.zip

Double click BrowserStackLocal.exe to start BrowserStackLocal

Start BrowserStackLocal

Run tests on browserstack
```
npm run test-bs
```

var edge = require('selenium-webdriver/edge');

exports.config = {
  framework: "jasmine",
  jasmineNodeOpts: {
    defaultTimeoutInterval: 120000,
  },
  restartBrowserBetweenTests: true,
  specs: ["./tests/testcases.login.spec.js"],

  multiCapabilities: [
    // {
    //   browserName: "chrome",
    //   chromeOptions: {
    //     args: ["--headless", "--disable-gpu", "--window-size=800x600"]
    //   },
    //   directConnect: true,
    // },
    // {
    //   browserName: 'MicrosoftEdge',
    //   elementScrollBehavior: 1,
    //   nativeEvents: false,
    //   seleniumAddress: 'http://localhost:4444/wd/hub',
    //   // edgeOptions: {
    //   //   args: ["--headless", "--disable-gpu", "--window-size=800x300"]
    //   // },
    // }
    {
      browserName: "firefox",
      // specs: ["./tests/testcases.login.spec.js"],

      //only required for me/Arpi on my laptop - since firefox is not installed at the default location
      "moz:firefoxOptions": {
        binary: "C:\\Users\\ac22\\AppData\\Local\\Mozilla Firefox\\firefox.exe"
        // binary: "C:\\Users\\aaug\\AppData\\Local\\Mozilla Firefox\\firefox.exe"
      }
    }
  ],

  plugins: [
    {
      package: "protractor-screenshoter-plugin",
      screenshotPath: "./Reports/e2e/local",
      screenshotOnExpect: "failure+success",
      screenshotOnSpec: "none",
      withLogs: true,
      writeReportFreq: "asap",
      imageToAscii: "none",
      clearFoldersBeforeTest: true,
    },
  ],

  onPrepare: function () {
    // let options = new edge.Options();
    // console.log('edge capabilities');
    // console.dir(options.toCapabilities());

    // returning the promise makes protractor wait for the reporter config before executing tests
    return global.browser.getProcessedConfig().then(function (config) {
      //it is ok to be empty
    });
  }
};
